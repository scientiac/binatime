use chrono::DateTime;
use chrono::Local;
use chrono::Timelike;
use figlet_rs::FIGfont;

fn insert_space_after_each_character(original_string: &str) -> String {
    let char_iterator = original_string.chars();
    let modified_chars = char_iterator.flat_map(|c| vec![c, ' ']);
    let modified_string: String = modified_chars.collect();
    let trimmed_string = modified_string.trim_end().to_string();
    trimmed_string
}

fn convert_to_ascii(hour_binary: &str) -> Option<String> {
    let binary_blocks_font = include_str!("../resources/binary_blocks.flf");
    let standard_font = FIGfont::from_content(binary_blocks_font).unwrap();

    let hour_ascii = standard_font.convert(hour_binary);
    hour_ascii.map(|result| result.to_string())
}

fn print_time(binary: &str, padding: &str) {
    match convert_to_ascii(binary) {
        Some(result) => {
            let padded_result: String = result
                .lines()
                .map(|line| format!("{}{}", padding, line))
                .collect::<Vec<_>>()
                .join("\n");
            println!("{}", padded_result);
        }
        None => println!("Conversion failed."),
    }
}

fn main() {
    loop {
        // Clear the terminal before execution.
        std::process::Command::new("clear").status().unwrap();

        // Print the hour and minute in binary to the terminal.
        let (width, height) = termion::terminal_size().unwrap();
        let vspaces = (height.saturating_sub(37)) / 2; // Use saturating_sub to avoid negative values
        let hspaces = (width.saturating_sub(50)) / 2; // Use saturating_sub to avoid negative values

        // If vspaces or hspaces are less than zero, set them to zero
        let vspaces = vspaces.max(1);
        let hspaces = hspaces.max(1);

        let padding: String = std::iter::repeat(' ').take(hspaces as usize).collect();

        for _ in 0..vspaces {
            println!("\n");
        }

        // Get the current time.
        let now: DateTime<Local> = Local::now();

        // Calculate the hour and minute in binary.
        let hour_binary_no_spaces = format!("{:06b}", now.hour() % 12);
        let minute_binary_no_spaces = format!("{:06b}", now.minute());
        let second_binary_no_spaces = format!("{:06b}", now.second());

        // Space out the result in bunary.
        let hour_binary = insert_space_after_each_character(&hour_binary_no_spaces);
        let minute_binary = insert_space_after_each_character(&minute_binary_no_spaces);
        let second_binary = insert_space_after_each_character(&second_binary_no_spaces);

        // Print the hour and minute in binary ASCII to the terminal.
        print_time(&hour_binary, &padding);
        println!("\n");
        print_time(&minute_binary, &padding);
        println!("\n");
        print_time(&second_binary, &padding);

        // Hide the cursor
        print!("{}", termion::cursor::Hide);

        // Wait for one second.
        std::thread::sleep(std::time::Duration::from_secs(1));
    }
}
